package com.neoflex.proizvodstvenniicalendar.service;

import com.neoflex.proizvodstvenniicalendar.model.CalendarOfHolidays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class CalendarServiceImplTest {

    @Autowired
    private CalendarServiceImpl calendarServiceImpl;

    @Autowired
    private CalendarOfHolidays calendarOfHolidays;

    @Test
    void calculateHolidayPay() {
        Assertions.assertEquals(BigDecimal.valueOf(500),
                calendarServiceImpl.calculateHolidayPay(BigDecimal.valueOf(100), 5));
    }

    @Test
    void calculateHolidayPayWithPeriod() {
        Map<String, List<LocalDate>> mapOfDates = new HashMap<>();
        mapOfDates.put("2022", calendarServiceImpl.getHolidaysCalendar("2022"));
        calendarOfHolidays.setMapOfDates(mapOfDates);
        Assertions.assertEquals(BigDecimal.valueOf(500),
                calendarServiceImpl.calculateHolidayPayWithPeriod(BigDecimal.valueOf(100),
                        LocalDate.parse("2022-10-03"), LocalDate.parse("2022-10-09")));
    }
}