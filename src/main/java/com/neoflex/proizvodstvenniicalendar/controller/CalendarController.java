package com.neoflex.proizvodstvenniicalendar.controller;

import com.neoflex.proizvodstvenniicalendar.service.CalendarServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RequiredArgsConstructor
@RestController
public class CalendarController {

    private final CalendarServiceImpl calendarServiceImpl;

    @GetMapping("/calculate")
    public BigDecimal calculateHolidayPay(@RequestParam("average-salary") BigDecimal averageSalary,
                                          @RequestParam(value = "days", required = false) Integer days,
                                          @RequestParam(value = "start", required = false) String start,
                                          @RequestParam(value = "end", required = false) String end) {
        return calendarServiceImpl.validateRequest(averageSalary, days, start, end);
    }
}
