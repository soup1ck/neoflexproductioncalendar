package com.neoflex.proizvodstvenniicalendar.model;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Component
@Data
public class CalendarOfHolidays {

    private Map<String, List<LocalDate>> mapOfDates;
}
