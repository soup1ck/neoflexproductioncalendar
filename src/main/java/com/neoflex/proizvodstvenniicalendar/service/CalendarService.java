package com.neoflex.proizvodstvenniicalendar.service;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface CalendarService {

    BigDecimal calculateHolidayPay(BigDecimal averageSalary, Integer days);
    BigDecimal calculateHolidayPayWithPeriod(BigDecimal averageSalary, LocalDate start, LocalDate end);
}
